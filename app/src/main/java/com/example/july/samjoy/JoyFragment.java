package com.example.july.samjoy;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by uzir on 05.11.2016.
 */
public class JoyFragment extends Fragment {

    private AxisParam[] axis_list;
    private ButtonParam[] button_list;
    private Client client;
    private boolean is_init;
    protected View view;


    public JoyFragment(){}

    public void setClient(Client c){
        client = c;
    }

    protected void setControls(AxisParam[] a, ButtonParam[] b){
        axis_list = a;
        button_list = b;
    }

    protected void startJoy(){
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        loadPrefs(getActivity().getPreferences(Context.MODE_PRIVATE));

        is_init = true;
    }

    protected void loadPrefs(SharedPreferences prefs){
        Log.i("debug","loadPrefs");
        SeekBar seekBar;

        for(int i = 0; i < axis_list.length; i++) {
            int p;
            TextView t;
            p = prefs.getInt("SeekBar" + Integer.toString(i + 1) + "Progress", 0);
            SBListener sbl = new SBListener();
            seekBar = (SeekBar) view.findViewById(axis_list[i].resource_id);
            seekBar.setProgress(p);
            seekBar.setOnSeekBarChangeListener(sbl);
            axis_list[i].setX(p);
            t = (TextView) view.findViewById(axis_list[i].value_resource_id);
            t.setText(Integer.toString(p));
        }
        for(int i = 0; i < button_list.length; i++) {
            Button button = (Button)view.findViewById(button_list[i].resource_id);
            if(button == null)
                continue;

            button.setOnTouchListener(new TListener());
        }

        for (int i = 0; i<axis_list.length; i++){
            send(axis_list[i].getBytes());
        }
    }

    protected void savePrefs(SharedPreferences prefs) {
        Log.i("debug","savePrefs");
        SharedPreferences.Editor editor = prefs.edit();

        for (int i = 0; i < axis_list.length; i++) {
            SeekBar seekBar = (SeekBar) view.findViewById(axis_list[i].resource_id);
            editor.putInt("SeekBar" + Integer.toString(i + 1) + "Progress", seekBar.getProgress());
        }
        editor.commit();
    }

    class SBListener implements SeekBar.OnSeekBarChangeListener{
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress,
                                      boolean fromUser) {
            int id = seekBar.getId();
            for (int i = 0; i<axis_list.length; i++){
                if (id == axis_list[i].resource_id){
                    TextView t = (TextView)view.findViewById(axis_list[i].value_resource_id);
                    t.setText(Integer.toString(progress));
                    progress *= 100.0f / seekBar.getMax();
                    axis_list[i].setX(progress);
                    send(axis_list[i].getBytes());
                }
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            Log.i("debug","onstarttracking");
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            Log.i("debug","onstoptracking");

        }
    }

    class TListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int id = v.getId();
            int index = -1;
            for (int i = 0; i < button_list.length; i++){
                if (id == button_list[i].resource_id){
                    index = i;
                }
            }
            if (index==-1) return false;
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: // нажатие
                    Log.i("debug","push");
                    send(button_list[index].getBytes(1));
                    break;
                case MotionEvent.ACTION_UP: // отпускание
                case MotionEvent.ACTION_CANCEL:
                    Log.i("debug","release");
                    send(button_list[index].getBytes(0));
                    break;
            }
            return false;
        }
    }

    class CListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            int index = -1;
            for (int i = 0; i < button_list.length; i++){
                if (id == button_list[i].resource_id){
                    index = i;
                }
            }
            if (index==-1) return;

            send(button_list[index].getBytes(1));

            return;
        }
    }

    protected void send(byte[] bytes){
        client.send(bytes);
    }


    protected void resendAxis(){
        for (int i = 0; i<axis_list.length; i++){
            send(axis_list[i].getBytes());
        }
    }


    public void onStop() {
        super.onStop();

        savePrefs(getActivity().getPreferences(Context.MODE_PRIVATE));

        if(client != null) client.stop();
    }


    public void onPause() {
        super.onPause();

        savePrefs(getActivity().getPreferences(Context.MODE_PRIVATE));
    }


    public void onResume(){
        super.onResume();

        loadPrefs(getActivity().getPreferences(Context.MODE_PRIVATE));

        resendAxis();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(!is_init)
            return;

        if(isVisibleToUser)
            loadPrefs(getActivity().getPreferences(Context.MODE_PRIVATE));
        else
            savePrefs(getActivity().getPreferences(Context.MODE_PRIVATE));
    }
}