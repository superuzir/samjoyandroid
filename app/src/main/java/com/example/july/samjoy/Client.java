package com.example.july.samjoy;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by july on 11/7/15.
 */
public class Client {
    static private Socket socket;
    static private DatagramSocket socket_bc;

    private static final int SERVER_PORT = 22000;
    private static final int BC_PORT = 22001;
    private String server_ip;

    private AliveTimerTask alive_timer_task;
    private Timer alive_timer;

    private Context context;

    public Client(Context c) {
        context = c;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }



    public boolean connect(){
        WifiManager wifi = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        if (wifi == null) return false;

        WifiManager.MulticastLock lock = wifi.createMulticastLock("mylock");
        lock.acquire();

        try {
            socket_bc = new DatagramSocket(BC_PORT);
            socket_bc.setBroadcast(true);

            byte[] buf = new byte[1024];
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            socket_bc.setSoTimeout(5000);
            socket_bc.receive(packet);

            String s = new String(packet.getData());
            server_ip = s.trim();

            socket_bc.close();

            Log.i("debug", server_ip);
        } catch (SocketTimeoutException e) {
            Log.i("debug", "timed out while retrieving server IP address from broadcast");
            socket_bc.close();
            return false;
        } catch (IOException e) {
//            e.printStackTrace();
            Log.i("debug", "io exception while retrieving server IP address from broadcast");
            return false;
        }

        lock.release();

        socket = new Socket();

        try{
            socket.setSoTimeout(5000);
        }
        catch(SocketException e){
//            e.printStackTrace();
            Log.i("debug", "socket setup error");
            return false;
        }
        try {
            Log.i("debug", "trying to connect client socket");
            if (!socket.isConnected()){
                Log.i("debug", "socket was not connected");
                socket.connect(new InetSocketAddress(server_ip, SERVER_PORT), 5000);
            }

            alive_timer = new Timer();
            alive_timer_task = new AliveTimerTask();
            alive_timer.schedule(alive_timer_task, 0, 1000);
        }
        catch (SocketTimeoutException e) {
//            e.printStackTrace();
            Log.i("debug","timeout exception");
            return false;
        }
        catch (UnknownHostException e) {
//            e.printStackTrace();
            Log.i("debug", "unknown host exception");
            return false;
        } catch (IOException e) {
//            e.printStackTrace();
            Log.i("debug", "io exception");
            return false;
        }

        return true;
    }

    public void stop(){
        if(alive_timer != null) alive_timer.cancel();
    }

    static boolean checkNetwork(){
        if ( socket == null) return false;
        if (!socket.isConnected()) return false;
        return true;
    }

    static void send(byte[] bytes) {
        if(!checkNetwork()) return;
        try {
            BufferedOutputStream out = new BufferedOutputStream(socket.getOutputStream());
            out.write(bytes);
            out.flush();
        } catch (IOException e) {
            Log.i("debug", "io exception");
            e.printStackTrace();
        }
    }

    class AliveTimerTask extends TimerTask {
        @Override
        public void run() {
            ByteBuffer byteBuffer = ByteBuffer.allocate(12);
            byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
            byteBuffer.putInt(100);
            byteBuffer.putInt(0);
            byteBuffer.putInt(0);

            send(byteBuffer.array());
        }
    }

}
