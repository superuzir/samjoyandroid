package com.example.july.samjoy;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by july on 11/5/15.
 */
public class AxisParam {
    int resource_id;
    int value_resource_id;
    int num;
    float k;
    float x;
    float b;
    public AxisParam(int id, int value_id, int num, float k, float b){
        this.resource_id = id;
        this.value_resource_id = value_id;
        this.num = num;
        this.k = k;
        this.b = b;
    }
    public byte[] getBytes(){
        ByteBuffer byteBuffer = ByteBuffer.allocate(12);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        byteBuffer.putInt(10);
        byteBuffer.putInt(num);
        byteBuffer.putInt(Math.round(k*x+b));
        return byteBuffer.array();
    }
    public void setX(float x_){
        x = x_;
    }
}