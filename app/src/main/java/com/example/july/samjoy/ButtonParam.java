package com.example.july.samjoy;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by july on 11/4/15.
 */
public class ButtonParam {
    int resource_id;
    int num;
    boolean continuous;
    public ButtonParam(int id, int num){
        this.resource_id = id;
        this.num = num;
        this.continuous = false;
    }
    public ButtonParam(int id, int num, boolean continuous){
        this.resource_id = id;
        this.num = num;
        this.continuous = continuous;
    }
    public byte[] getBytes(int state){
        ByteBuffer byteBuffer = ByteBuffer.allocate(12);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        if (continuous) {
            byteBuffer.putInt(2);
        }
        else {
            byteBuffer.putInt(1);
        }
        byteBuffer.putInt(num);

        byteBuffer.putInt(state);
        return byteBuffer.array();
    }
}

