package com.example.july.samjoy;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;

/**
 * Created by uzir on 14.11.2015.
 */
public class ConnectActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);
    }

    public void onClick(View v) {
        int id = v.getId();
        Intent intent;
        switch (id) {
            case R.id.buttonGame1:
                intent = new Intent(this, TabbedActivity.class);
                startActivity(intent);
                return;
        }
    }
}
