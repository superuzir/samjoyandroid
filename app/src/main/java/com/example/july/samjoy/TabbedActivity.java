package com.example.july.samjoy;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;


public class TabbedActivity extends AppCompatActivity {

    private AxisParam[] axis_list = {
            new AxisParam(R.id.seekBar1, R.id.textViewValue1, 1, 32767f / 100f, 0),
            new AxisParam(R.id.seekBar2, R.id.textViewValue2, 2, 32767f / 100f, 0),
            new AxisParam(R.id.seekBar3, R.id.textViewValue3, 3, 32767f / 100f, 0),
            new AxisParam(R.id.seekBar4, R.id.textViewValue4, 4, 32767f / 100f, 0),
            new AxisParam(R.id.seekBar5, R.id.textViewValue5, 5, 32767f / 100f, 0),
            new AxisParam(R.id.seekBar6, R.id.textViewValue6, 6, 32767f / 100f, 0),
    };

    private ButtonParam[] button_list = {
            new ButtonParam(R.id.buttonTrackIR, 1),
            new ButtonParam(R.id.buttonAgOnoff, 2),
            new ButtonParam(R.id.buttonGearsUp, 3),
            new ButtonParam(R.id.buttonGearsDown, 4),
            new ButtonParam(R.id.buttonEngineStart, 5),
            new ButtonParam(R.id.buttonCanopy, 6),
            new ButtonParam(R.id.buttonEject, 7),
            new ButtonParam(R.id.buttonForsage, 8),
            new ButtonParam(R.id.buttonGearsAuto, 9),
            new ButtonParam(R.id.buttonAutopilot, 10),
            new ButtonParam(R.id.buttonLandingLights, 11),
            new ButtonParam(R.id.buttonLights, 12),
            new ButtonParam(R.id.buttonMap, 13),
            new ButtonParam(R.id.buttonPanelLights, 14),
            new ButtonParam(R.id.buttonPause, 15),
            new ButtonParam(R.id.buttonMapFull, 16),
            new ButtonParam(R.id.buttonRecord, 17),
            new ButtonParam(R.id.buttonReload, 18),
            new ButtonParam(R.id.buttonBoostUp, 19),
            new ButtonParam(R.id.buttonBoostDown, 20),
            new ButtonParam(R.id.buttonUser1, 21),
            new ButtonParam(R.id.buttonUser2, 22),
            new ButtonParam(R.id.buttonUser3, 23),
            new ButtonParam(R.id.buttonUser4, 24),
            new ButtonParam(R.id.buttonStats, 25, true),

            new ButtonParam(R.id.buttonSmoke, 26),
            new ButtonParam(R.id.buttonFlareLights, 62),

            new ButtonParam(R.id.buttonEngine1, 27),
            new ButtonParam(R.id.buttonEngine2, 28),
            new ButtonParam(R.id.buttonEngine3, 29),
            new ButtonParam(R.id.buttonEngine4, 30),
            new ButtonParam(R.id.buttonEngineAll, 36),

            new ButtonParam(R.id.buttonBombDoor, 31),
            new ButtonParam(R.id.buttonBombMode, 34),
            new ButtonParam(R.id.buttonBombDelay, 38),
            new ButtonParam(R.id.buttonBombSeries, 46),
            new ButtonParam(R.id.buttonBombSightUp, 48, true),
            new ButtonParam(R.id.buttonBombSightDown, 50, true),
            new ButtonParam(R.id.buttonBombSightRight, 51, true),
            new ButtonParam(R.id.buttonBombSightLeft, 60, true),
            new ButtonParam(R.id.buttonBombSightSpeedUp, 59, true),
            new ButtonParam(R.id.buttonBombSightSpeedDown, 52, true),
            new ButtonParam(R.id.buttonBombSightAltitudeUp, 54, true),
            new ButtonParam(R.id.buttonBombSightAltitudeDown, 56, true),
            new ButtonParam(R.id.buttonBombSightCalculate, 53),

            new ButtonParam(R.id.buttonRocketMode, 45),
            new ButtonParam(R.id.buttonSiren, 32),
            new ButtonParam(R.id.buttonDiveBrake, 37),

            new ButtonParam(R.id.buttonRpmAuto, 33),
            new ButtonParam(R.id.buttonMixtureAuto, 47),
            new ButtonParam(R.id.buttonPropellerPitchAuto, 58),
            new ButtonParam(R.id.buttonRadiatorAuto, 61),
            new ButtonParam(R.id.buttonAutopilotAutoenable, 55),
            new ButtonParam(R.id.buttonPropellerStreamLine, 57),

            new ButtonParam(R.id.buttonParkingBrake, 49),
            new ButtonParam(R.id.buttonTailWheel, 35),

            new ButtonParam(R.id.buttonPosition1, 39),
            new ButtonParam(R.id.buttonPosition2, 40),
            new ButtonParam(R.id.buttonPosition3, 41),
            new ButtonParam(R.id.buttonPosition4, 42),
            new ButtonParam(R.id.buttonPosition5, 43),
            new ButtonParam(R.id.buttonPosition6, 44),
    };

    private Client client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbed);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        ViewPager viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        if(client == null) {
            client = new Client(this);
            if(!client.connect()){
                Log.i("debug", "Could not connect");
                Toast.makeText(getApplicationContext(), "Connection failed!", Toast.LENGTH_LONG).show();

                this.finish();
            } else {
                Log.i("debug", "Connection OK");
                Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_LONG).show();
            }
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    TakeoffJoyFragment f = new TakeoffJoyFragment();
                    f.setClient(client);
                    f.setControls(axis_list, button_list);
                    return f;
                }

                case 1: {
                    FlightJoyFragment f = new FlightJoyFragment();
                    f.setClient(client);
                    f.setControls(axis_list, button_list);
                    return f;
                }

                case 2: {
                    BombingJoyFragment f = new BombingJoyFragment();
                    f.setClient(client);
                    f.setControls(axis_list, button_list);
                    return f;
                }
            }

            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Взлет/посадка";
                case 1:
                    return "Полет";
                case 2:
                    return "Бомбометание";
            }
            return null;
        }
    }
}
